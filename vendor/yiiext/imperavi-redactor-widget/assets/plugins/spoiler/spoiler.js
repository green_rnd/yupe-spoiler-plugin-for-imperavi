/*
 Spoiler plugin for imperavi editor and Yupe CMS
 Andrey Balyasov
 green.rnd@ya.ru
 */
if (!RedactorPlugins) var RedactorPlugins = {};

(function ($) {
    RedactorPlugins.spoiler = function () {
        return {
            init: function () {
                var that = this;
                console.log("spoiler init");
                var button = this.button.add('spoiler', 'Spoiler');
                this.button.addCallback(button, this.spoiler.show);
            },
            show: function()
            {
                this.code.sync();
                this.selection.get();
                this.spoiler.startEl = this.range.startContainer;
                this.spoiler.endEl = this.range.endContainer;
                this.spoiler.commonEl = this.range.commonAncestorContainer

                this.modal.addTemplate('spoiler', this.spoiler.getTemplate());

                this.modal.load('spoiler', this.lang.get('insert_table'), 300);
                this.modal.createCancelButton();

                var button = this.modal.createActionButton(this.lang.get('insert'));
                button.on('click', this.spoiler.setSpoiler);

                this.selection.save();
                this.modal.show();

                $('#redactor-table-rows').focus();

            },
            setSpoiler: function () {
                this.modal.close();

                this.code.sync();

                let {startEl, endEl, commonEl} = this.spoiler;

                const spoilerStart = '[spoiler title="' + $('#redactor-spoiler-title').val() + '"]';
                const spoilerEnd = '[/spoiler]';

                $('#selection-marker-1').before(spoilerStart);
                $('#selection-marker-2').after(spoilerEnd);

                this.code.sync();
                this.selection.restore();
            },
            getTemplate: function()
            {
                return String()
                    + '<section id="redactor-modal-table-insert">'
                    + '<label>Заголовок</label>'
                    + '<input type="text" size="100" value="Спойлер" id="redactor-spoiler-title" />'
                    + '</section>';
            },
        };
    };
})(jQuery);