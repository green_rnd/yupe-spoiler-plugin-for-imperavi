# README #

Yupe spoiler/hide plugin for imperavi editor

### How do I get set up? ###

* Скопировать \vendor\yiiext\imperavi-redactor-widget\assets\plugins\spoiler в соответствующую директорию

* В \protected\modules\yupe\widgets\editors\Redactor.php в методе getPlugins добавить в массив 
~~~~
    'spoiler' => [
                    'js' => ['spoiler.js']
                ],
~~~~

* В \vendor\yiiext\imperavi-redactor-widget\ImperaviRedactorWidget.php в методе registerClientScript в массив ```'css' => array()``` добавить
~~~~
'/plugins/spoiler/spoiler.css'
~~~~

* В \protected\modules\yupe\components\controllers\FrontController.php переопределить метод renderPartial

~~~~
public function renderPartial($view,$data=null,$return=false,$processOutput=false)
    {
        if(($viewFile=$this->getViewFile($view))!==false)
        {
            $output=$this->renderFile($viewFile,$data,true);

            if($processOutput){
                $output=$this->processOutput($output);
            }

            $count = 0;
            $output = preg_replace_callback('/\[spoiler]/',
                function ($matches) {
                    global $count;
                    $button = '<button class="btn btn-primary btn-collapse collapsed" type="button" data-toggle="collapse" ' .
                        'data-target="#collapse_' . $count . '" aria-expanded="false" aria-controls="collapse_' . $count . '">' .
                        '</button>';
                    $collapse = '<div class="collapse" id="collapse_' . $count . '"><div class="well">';
                    $count++;
                    return $button . $collapse;
                }, $output);

            $collapse_end = '</div></div>';
            $output = str_replace('[/spoiler]', $collapse_end, $output);
            /*$output = str_replace('[spoiler]', $button . $collapse, $output);
            */

            if($return)
                return $output;
            else
                echo $output;
        }
        else
            throw new CException(Yii::t('yii','{controller} cannot find the requested view "{view}".',
                array('{controller}'=>get_class($this), '{view}'=>$view)));
    }
~~~~

* Для изменения текста кнопок при открытии/закрытии спойлера в css добавить
~~~~
button.btn-collapse.collapsed:before
{
    content:'+' ;
    display:block;
    width:15px;
}
button.btn-collapse:before
{
    content:'-' ;
    display:block;
    width:15px;
}
~~~~

* Сбросить кэш Yupe