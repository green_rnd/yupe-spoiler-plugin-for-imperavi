<?php
/**
 * Базовый класс для всех контроллеров публичной части
 *
 * @category YupeComponents
 * @package  yupe.modules.yupe.components.controllers
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @version  0.6
 * @link     http://yupe.ru
 **/

namespace yupe\components\controllers;

use Yii;
use yupe\events\YupeControllerInitEvent;
use yupe\events\YupeEvents;
use application\components\Controller;

/**
 * Class FrontController
 * @package yupe\components\controllers
 */
abstract class FrontController extends Controller
{
    public $mainAssets;

    /**
     * Вызывается при инициализации FrontController
     * Присваивает значения, необходимым переменным
     */
    public function init()
    {
        Yii::app()->eventManager->fire(YupeEvents::BEFORE_FRONT_CONTROLLER_INIT, new YupeControllerInitEvent($this, Yii::app()->getUser()));

        parent::init();

        Yii::app()->theme = $this->yupe->theme ?: 'default';

        $this->mainAssets = Yii::app()->getTheme()->getAssetsUrl();

        $bootstrap = Yii::app()->getTheme()->getBasePath() . DIRECTORY_SEPARATOR . "bootstrap.php";

        if (is_file($bootstrap)) {
            require $bootstrap;
        }
    }

    public function renderPartial($view,$data=null,$return=false,$processOutput=false)
    {
        if(($viewFile=$this->getViewFile($view))!==false)
        {
            $output=$this->renderFile($viewFile,$data,true);

            if($processOutput){
                $output=$this->processOutput($output);
            }

            $count = 0;
            $output = preg_replace_callback('/(\[spoiler title=\".*?\"\])/',
                function ($matches) {
                    global $count;
                    $title_raw = preg_match('/title=\"(.*)\"/', $matches[0], $result);
                    $button = '<div class="btn btn-primary btn-collapse collapsed" type="button" data-toggle="collapse" ' .
                        'data-target="#collapse_' . $count . '" aria-expanded="false" aria-controls="collapse_' . $count . '">' .
                        htmlspecialchars_decode($result[1]).
                        '</div>';
                    $collapse = '<div class="collapse" id="collapse_' . $count . '"><div class="well">';
                    $count++;
                    return $button . $collapse;
                }, $output);

            $collapse_end = '</div></div>';
            $output = str_replace('[/spoiler]', $collapse_end, $output);

            if($return)
                return $output;
            else
                echo $output;
        }
        else
            throw new CException(Yii::t('yii','{controller} cannot find the requested view "{view}".',
                array('{controller}'=>get_class($this), '{view}'=>$view)));
    }
}
